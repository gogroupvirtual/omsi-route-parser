# Requirements
- Python

# How to use 
```bash
python.exe .\convertTTDataToMarkdowntable.py -f "path-to-map-TTData-folder"
```
# Example
```bash
python.exe .\convertTTDataToMarkdowntable.py -f "D:\coding-projects\gogroup\data-omsi\data\core\Liestal V3\TTData"
```