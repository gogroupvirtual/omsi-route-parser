import os
import glob
import argparse

parser = argparse.ArgumentParser(description="Convert OMSI route data to Markdown Table")
parser.add_argument("-f", "--folder", help="Data folder", required=True)
args = parser.parse_args()

# Escape the folder path to handle brackets and other special characters
escaped_folder = glob.escape(args.folder)  # Escapes any special glob characters in the folder path

for filename in glob.glob(os.path.join(escaped_folder, '*.ttl')):
    file = os.path.basename(filename).removesuffix('.ttl')
    with open(os.path.join(os.getcwd(), filename), 'r') as readfile:  # open in readonly mode
        for line in readfile:
            if "[newtour]" in line:
                print("| " + file + " | " + str(next(readfile).rstrip()) + " | " + str(next(readfile)).rstrip())
